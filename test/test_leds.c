#include "unity.h"
#include "leds.h"
#include <string.h>

//#include "mock_errorsLib.h" //comment to avoid Cmock to create a definition of RegisterErrorMessage

static uint16_t ledsVirtuales;

static struct {
   int errorLevel;
   char function[64];
} error;


/* This function is just for the test, explicit definition of the function. Stores the errorLevel and function name inside struct 'error', 
* used to check that error code and message is correctly setted.
*/
void RegisterErrorMessage(int errorLevel, const char* functionName, int line, const char* errorMsg){
   error.errorLevel = errorLevel;
   strcpy( error.function, functionName); //save function name into error struct.
}

void clearRegisterErrorMessage(void){
   error.errorLevel = NO_ERROR;
   memset( error.function, 0, sizeof(error.function)); //clear the buffer
}

void setUp(void){
   clearRegisterErrorMessage();
   LedCreate(&ledsVirtuales, RegisterErrorMessage); 
}

void tearDown(void){
}

/**
 * @brief Test Case: Check that after creating the LED driver, all LEDS start with the off state (bit value for each LED is 0).
 */
void test_LedsOffAfterCreate(void){
   uint16_t ledsVirtuales = 0xFFFF; //Force led register to be on High in order to test that after LedCreate is call, the values change to 0.
   LedCreate(&ledsVirtuales, RegisterErrorMessage);
   TEST_ASSERT_EQUAL_HEX16(0, ledsVirtuales);
}

/**
 * @brief Test Case: Turns On Led 1, check that it's corresponding bit is setted correctly.
 * 
 */
void test_TurnOnLedOne(void)
{
   LedTurnOn(1); //turn On led 1
   TEST_ASSERT_EQUAL_HEX16(1, ledsVirtuales);
}

/**
 * @brief Test Case: Checks that the function 'LedTurnOff' works correctly, first LED 1 is turned ON and after that 'LedTurnOff' is called to check that LED changes
 * to OFF state.
 */
void test_TurnOffLedOne(void)
{
   LedTurnOn(1);
   LedTurnOff(1);
   TEST_ASSERT_EQUAL_HEX16(0, ledsVirtuales);
}

/**
 * @brief Test case: Check that the first ID status (bit 0) correspond to the LSB bit of the 
 * uint16_t ledVertuales word.
 * 
 */
void test_CheckFirstLedIsLSB(void){
   LedTurnAllOff(); //turn off all leds to avoid problems
   LedTurnOn(1); //turn on the first led
   TEST_ASSERT_TRUE( 0x01 & ledsVirtuales ); //true if led 1 is in LSB bit
}

/**
 * @brief Test case: Get the status of a given LED.
 * 
 */
void test_GetTheStatusOfAnLed(){
   LedTurnOn(1); //turn on the first LED.
   TEST_ASSERT_TRUE( LedGetStatusOfLed(1) );
}

/**
 * @brief Test case: retrieves the status of a led that is OFF.
 * 
 */
void test_GetTheStatusOfAnLedWhichIsOff(){
   LedTurnOff(1); //turn on the first LED.
   TEST_ASSERT_FALSE( LedGetStatusOfLed(1) );
}

/**
 * @brief Test case: Check that when a given LED (in this example led 5) is turned on
 * it doesn't affect the status of the rest LED. 
 * For this test, as an initial context, the LED 1,2,7,8 will be ON and the rest off.
 * 
 */
void test_CheckThatWhenALedIsTurnedOnItDoesntModifiyOtherLedsStates(void){
   LedTurnAllOff();
   LedTurnOn(1);
   LedTurnOn(2);
   LedTurnOn(7);
   LedTurnOn(8);
   LedTurnOn(5);
   uint16_t expectedResult = 0b0000000011010011;
   TEST_ASSERT_EQUAL_HEX16( expectedResult, ledsVirtuales);
}


/**
 * @brief Test case: Turn on all the leds as a sequence, after the sequence finish,
 * the uint16t variable LedVirtuales should be 0xFFFF (indicating all leds in high state).
 * 
 */
void test_TurnOnLedInSequenceModeShouldSetLedVirtualesVarToitsMaxValue(void){
   LedTurnAllOff();
   char msgPool[64];

   for(int ledIndex = 1; ledIndex <= 16; ledIndex++){
      LedTurnOn(ledIndex);
      snprintf( (char*)msgPool, sizeof(msgPool), "Led index: %i\n", ledIndex);
      TEST_ASSERT_TRUE_MESSAGE( LedGetStatusOfLed(ledIndex), msgPool);
   }

   snprintf( (char*)msgPool, sizeof(msgPool), "LedsVirtuales value: 0x%04x\n", ledsVirtuales);
   TEST_ASSERT_EQUAL_HEX16_MESSAGE( 0xFFFF, ledsVirtuales, msgPool);
}


/**
 * @brief Test case: Test led 1 and 16, for this the following subcases will be tested:
 *    -       | Led 1 | Led 16 |
 *            |*******|********|
 *    -case 1 |  On   |  On    |
 *     case 2 |  On   |  Off   |
 *     case 3 |  Off  |  Off   |
 *     case 4 |  Off  |  On    |
 */
void test_TurnOnAndOffLeds1And16(void){
   char msgPool[64];
   LedTurnAllOff();
   //Case 1, led 1 on, led 16 off
   LedTurnOn(1);
   LedTurnOn(16);
   snprintf( (char*)msgPool, sizeof(msgPool), "Led 1 and 16 ON, LedsVirtuales value: 0x%04x\n", ledsVirtuales);   
   uint16_t expectedResult = 0b1000000000000001;
   TEST_ASSERT_EQUAL_HEX16_MESSAGE( expectedResult, ledsVirtuales, msgPool);

   //Case 2, led 1 on, led 16 off
   LedTurnOff(16);
   snprintf( (char*)msgPool, sizeof(msgPool), "Led 1 ON and 16 OFF, LedsVirtuales value: 0x%04x\n", ledsVirtuales);   
   expectedResult = 0b0000000000000001;
   TEST_ASSERT_EQUAL_HEX16_MESSAGE( expectedResult, ledsVirtuales, msgPool);

   //Case 3, led 1 off, led 16 off
   LedTurnOff(1);
   snprintf( (char*)msgPool, sizeof(msgPool), "Led 1 and 16 OFF, LedsVirtuales value: 0x%04x\n", ledsVirtuales);   
   expectedResult = 0b0000000000000000;
   TEST_ASSERT_EQUAL_HEX16_MESSAGE( expectedResult, ledsVirtuales, msgPool);
      
   //Case 4, led 1 off, led 16 on
   LedTurnOn(16);
   snprintf( (char*)msgPool, sizeof(msgPool), "Led 1 OFF and 16 ON, LedsVirtuales value: 0x%04x\n", ledsVirtuales);   
   expectedResult = 0b1000000000000000;
   TEST_ASSERT_EQUAL_HEX16_MESSAGE( expectedResult, ledsVirtuales, msgPool);
}

/**
 * @brief Test case: Invalid pin parameter shouldn't modify the ledVirtuales status array.
 *                   If values greater than 16 are used, it shouldn't modify the current status.
 *  
 */
void test_InvalidPinNumberShouldNotModifyLedVirtualesForTurnOn(void){
   //RegisterErrorMessage_Expect(0 , "LedTurnOn", 0, "Invalid Led Number");
   //RegisterErrorMessage_IgnoreArg_line(); //tell the mock to ignore the argument 'line'

   clearRegisterErrorMessage(); //clear error code flag and function name

   uint16_t expectedResult = 0;
   LedTurnAllOff();
   LedTurnOn(17);
   char msgPool[64];
   snprintf( (char*)msgPool, sizeof(msgPool), "LedsVirtuales value: 0x%04x\n", ledsVirtuales);   
   TEST_ASSERT_EQUAL_HEX16_MESSAGE(0, ledsVirtuales, msgPool); //Check that 'ledVirtuales' wasn't modified.

   TEST_ASSERT_EQUAL( ERROR_LED_NOT_EXIST, error.errorLevel);
   TEST_ASSERT_EQUAL_STRING("LedTurnOn", error.function); //check that RegisterErrorMessage function works properly when an invalid LED is turned on

}

/**
 * @brief Test case: Invalid pin parameter shouldn't modify the ledVirtuales status array.
 *                   If values greater than 16 are used, it shouldn't modify the current status.
 *  
 */
void test_InvalidPinNumberShouldNotModifyLedVirtualesForTurnOff(void){
   uint16_t expectedResult = 0;
   LedTurnAllOn();
   LedTurnOff(17);
   LedTurnOff(25);
   LedTurnOff(-1);
   char msgPool[64];
   snprintf( (char*)msgPool, sizeof(msgPool), "LedsVirtuales value: 0x%04x\n", ledsVirtuales);   
   TEST_ASSERT_EQUAL_HEX16_MESSAGE(0xFFFFFFFF, ledsVirtuales, msgPool);
}

/**
 * @brief Test case: Turn on multiple LEDS to check if their status is correctly saved on the LED register that is represented with 'ledVirtuales'
 * 
 */
void test_TurnOnMultipleLeds(void)
{
   LedTurnAllOff();
   LedTurnOn(8);
   LedTurnOn(9);
   TEST_ASSERT_EQUAL_HEX16(0x0180, ledsVirtuales); //check that only the LED 8 and LED 9 are turned on.
}

/**
 * @brief Test case: Turn off any type of LED, for this test all LEDS are turned ON, and led 8 is turned off.
 * 
 */
void test_TurnOffAnyLed(void)
{
   LedTurnAllOn();
   LedTurnOff(8);
   TEST_ASSERT_EQUAL_HEX16(0xFF7F, ledsVirtuales);
}

/**
 * @brief Test case:Turns On all the LEDs. Check that 'ledVirtuales' registers are set on HIGH.
 * 
 */
void test_TurnAllOn(void)
{
   LedTurnAllOn();
   TEST_ASSERT_EQUAL_HEX16(0xFFFF, ledsVirtuales);
}

/**
 * @brief Test case: Turn off all the LEDS with a single step.
 * 
 */
void test_TurnAllOff(void){
   LedTurnAllOn(); 
   LedTurnAllOff();
   TEST_ASSERT_EQUAL_HEX16(0x0000, ledsVirtuales);
}

void test_LedMemoryIsNotReadable(void)
{
   ledsVirtuales = 0xFFFF;
   LedTurnOn(8);
   TEST_ASSERT_EQUAL_HEX16(0x0080, ledsVirtuales);
}

void test_NullArgumentForLedCreateShouldFail(void){
   clearRegisterErrorMessage();
   uint16_t* ledRegister = NULL;
   LedCreate( ledRegister, RegisterErrorMessage);

   TEST_ASSERT_EQUAL( ERROR_NULL_LED_REGISTER, error.errorLevel); //Error Level
   TEST_ASSERT_EQUAL_STRING("LedCreate", error.function); //check that RegisterErrorMessage function works properly when an invalid LED is turned on
}