#ifndef LED_LIB_H
#define LED_LIB_H

#include <stdint.h>
#include "errorsLib.h"

#define MIN_LED_PIN_NUMBER 1
#define MAX_LED_PIN_NUMBER 16

/**
 * @brief Initializes the Led driver. 
 * 
 * @param direccion: pointer to the 16 bit variable that holds each of the 16 LED on/off status
 * @param logger: callback function to log Data when an error occurs
 * @return: -
 */
void LedCreate(uint16_t * direccion, logger_t logger);

/**
 * @brief Turns ON a single LED that is given in as a parameter. 
 * 
 * @param led : LED number which will be turned ON.
 * @return: -
 */
void LedTurnOn(int led);

/**
 * @brief Turns OFF a single LED that is given in as a parameter.
 * 
 * @param led LED number which will be turned OFF.
 * @return: -
 */
void LedTurnOff(int led);

/**
 * @brief Turns ON all the LEDS.
 * @param: -
 * @return:-
 */
void LedTurnAllOn(void);

/**
 * @brief Turns OFF all the leds
 * @param: -
 * @return: -
 */
void LedTurnAllOff(void);

/**
 * @brief Retrieves the status of a given Led passed as a parameter. 
 * 
 * @param led led that is going to be requested it's status.
 * @return 1 if led is on, 0 if it is off.
 */
uint8_t LedGetStatusOfLed(int led);

/**
 * @brief Converts a decimal number representing the LED number (ie: led 1 = 1) and converts it to the
 * bit position of the uint16_t variable that stores all the LED status data
 * 
 * @param ledNumber 
 * @return int 
 */
int LedConvertNumberToBit(int ledNumber);

#endif