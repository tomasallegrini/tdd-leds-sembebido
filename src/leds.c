#include "leds.h"


static uint16_t * leds;
static uint16_t estado;
static logger_t LogError; //pointer that stores the callback function

enum {
   ALL_LEDS_OFF = 0, 
   ALL_LEDS_ON = ~ALL_LEDS_OFF 
};

int LedConvertNumberToBit(int ledNumber) {
   return 1 << (ledNumber - 1);
}

void LedHardwareUpdate(void) {
   *leds = estado;
}

void LedCreate(uint16_t * direccion, logger_t logger) {

   if(direccion == 0x00){
      RegisterErrorMessage( ERROR_NULL_LED_REGISTER, __FUNCTION__, __LINE__, "Invalid register Address, NULL");
   }else{
      leds = direccion;
      LogError = logger;
      estado = ALL_LEDS_OFF;
      LedHardwareUpdate();  
   }
}

void LedTurnOn(int led) {
   //Check that led number is valid, in that case, perform the 'state' update
   if( (led >= MIN_LED_PIN_NUMBER) && (led <= MAX_LED_PIN_NUMBER) ){
      //we are in the presence of a valid LED number
      estado |= LedConvertNumberToBit(led);
      LedHardwareUpdate(); 
   }else{
      RegisterErrorMessage(ERROR_LED_NOT_EXIST, __FUNCTION__, __LINE__, "Invalid Led Number");
   }
}

void LedTurnOff(int led) {
   estado &= ~LedConvertNumberToBit(led);
   LedHardwareUpdate();   
}

void LedTurnAllOn(void) {
   estado = ALL_LEDS_ON;
   LedHardwareUpdate();   
}

void LedTurnAllOff(void){
   estado = ALL_LEDS_OFF;
   LedHardwareUpdate();
}

uint8_t LedGetStatusOfLed(int led){
   return *leds & led;
}

