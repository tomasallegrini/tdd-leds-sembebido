#ifndef ERROR_REGISRTY_H
#define ERROR_REGISRTY_H


/*############################# ERROR LEVEL DEFINITION #######################################################*/
#define NO_ERROR 0
#define ERROR_LED_NOT_EXIST 1
#define ERROR_NULL_LED_REGISTER 2
/*############################################################################################################*/
/**
 * @brief Registers the error msg (notifies the user when an error occurs)
 * 
 * @param errorLevel Type of error leve: 0 -> 
 *                                       1 ->
 * @param functionName Name of the function that fails. 
 * @param line  Indicates in which line the function has failed.
 * @param errorMsg Error msg for the user.
 */
void RegisterErrorMessage(int errorLevel, const char* functionName, int line, const char* errorMsg);

/**
 * @brief Callback
 * 
 * 
 */
typedef void (*logger_t)(int gravedad, const char *function, int linea, const char *mensaje);

#endif