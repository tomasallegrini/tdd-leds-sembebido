# README #

This README would normally document whatever steps are necessary to get your application up and running.

### **What is this repository for?** ###

* Simple Demo of using TDD workflow to develop an embeddes-system application using unit testing.
 The tools that are being used in this demo are Ceedling/Unity for developing and running the unit test, and gcovr to create reports with metrics such as branch coverage.
* Version: 1.0.0

* The requirements that this firmware must complied are the following:
  - Control both states (on/off) of 16 LEDs (encendido y apagado).
  - Change the state of an LED without altering the state of another LED.
  - Change the state of all LED with a single function call.
  - There must be a function to retrive the state of a given LED.
  - At start all LED must start with the off state.
  - ON state of a given LED is setted by putting in '1' the bit corresponding to that LED in the memory register. Off state by putting a '0' on that specific bit. 
  - LED 1 corresponds to LSB and LED 16 to MSB.

### **How do I get set up?** ###

* Summary of set up
* Configuration
* Dependencies
	- Ruby
	- Ceedling - Install using gem
	- gcov & gcovr - sudo apt install gcov
* Database configuration
* How to run tests
	To run Unit test + Coverage:
	Open terminal on the root folder of the proyect and run:
	1) Run 'ceedling clobber'   (to clean all the proyect mocks and coverage code that were created by cmock and gcov)
	2) Run 'ceedling gcov:all'  (to compile the code and generate the necessary code to perform the coverage)
	3) Run 'ceedling utils:gcovr' (to perform the coverage report, to open this reports go to: /tdd-leds/sembebido/build/artifacts/gcov/ and open the report).
	
	To simply run unit test:
	1) Run 'ceedling' in the terminal


* Deployment instructions

### **Contribution guidelines** ###

* Writing tests
* Code review
* Other guidelines

### **Who do I talk to?** ###

* Owner of repo: Tomas Agustin Allegrini - tallegrini74@gmail.com

